---- CHALLENGE ----

AUTOMAÇÃO DE TESTE BASE 

Requisitos de software e hardware necessários para executar este projeto de automação

- Java 1.8 SDK
- Maven
  
--- EXECUÇÃO ---

. No projeto retirar as tags do RunTest no package runner
. para executar somente 1 teste especifico adicionar tag do bdd com @ no package runner

. Botão direito e Run

Outra forma de execução

- mvn clean
- mvn install
- mvn verify

--- Autor ---
Thiago Souza
